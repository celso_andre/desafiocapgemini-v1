import React, { useState } from 'react';
import {Button, SafeAreaView, StyleSheet , Image, Text, View } from 'react-native'; 

const logoUri = './assets/logo.png';

export default () => {

    const [count, setCount] = useState(0);

    return (
        <SafeAreaView style = { style.App } >

            <View style = { style.View }>

                <Image
                    accessibilityLabel = "React Logo" 
                    source = {require(logoUri)} style = {style.ImageLogo}
                    resizeMode = "contain"
                />
                
                <Text style = { style.TextTitle } >
                    React Native
                </Text>

                <Text style = { style.TextCount } > 
                    Count: { count} 
                </Text>
                    
                <View style ={ style.ViewButtom }>
                    <Button
                        style = { style.Butom }
                        title = 'Counter Button'
                        onPress = { () => setCount(count + 1) }
                    />
                </View>
            </View>

        </SafeAreaView>
)};

const style = StyleSheet.create ({
    App:{
        backgroundColor: '#fff',
        flexGrow: 1,
        alignItems: "center",
        flexDirection: "column",
    },

    View:{
        marginTop: 25,
        paddingTop: 25,
        height: "80%",
        alignItems: "center",
        width: "100%",
    },
    
    ImageLogo: {
        width: "25%",
        height: "25%",
    }, 

    TextTitle: {
        fontWeight: "bold",
        fontSize: 22,
        marginTop: 20,
    },

    TextCount: {
        fontSize: 14,
        marginTop: 35,
    },

    ViewButtom: {
        width: "45%", 
        margin: 50
    },
    
    Butom:{
        marginTop:100,
    },


});